using System;
					
public class Program
{
	public static bool CheckDivisor(int i, int n){
		bool check = false;
		if(n % i == 0){
			check = true;
		}
		return check;
	}
	public static void Main()
	{
		int n;
		int sum;
		int i;
		sum = 0;
		i = 1;
		Console.WriteLine("Nhập số cần kiểm tra số hoàn hảo hay không?");
		n = Int32.Parse(Console.ReadLine());
		do{
			if(CheckDivisor(i, n)){
				sum += i;
			}
			i++;
		}
		while(i < n);
		if(sum == n){
		Console.WriteLine("Số hoàn hảo");
		}
		else{
		Console.WriteLine("Không phải số hoàn hảo");
		}
		Console.ReadKey();
	}
}