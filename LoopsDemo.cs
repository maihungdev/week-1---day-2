using System;
					
public class Program
{
	public static void MethodFor(int[] a){
		for(int i = 0; i < a.Length; i++){
			Console.WriteLine(a[i]);
		}
	}
	public static void MethodForeach(int[] a){
		foreach(var i in a){
			Console.WriteLine(i);
		}
	}
	public static void MethodDoWhile(int[] a){
		int i = 0;
		do{
			Console.WriteLine(a[i]);
			i++;
		}
		while(a.Length > i);
	}
	
	public static void Main()
	{
		Console.WriteLine("Loops For Demo");
		int[] listNumber = {1,3,5,7,9,2,4,6,8};
		MethodFor(listNumber);
		Console.WriteLine("Loops Foreach Demo");
		MethodForeach(listNumber);
		Console.WriteLine("Loops Do While");
		MethodDoWhile(listNumber);
		Console.ReadKey();
	}
}