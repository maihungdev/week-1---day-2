using System;
					
public class Program
{
	enum Days { Sun, Mon, tue, Wed, thu, Fri, Sat };
	public static void Main()
	{
		Console.WriteLine("Enum trong C#");
        Console.WriteLine("------------------------\n");
		int firstWeek = (int)Days.Mon;
		int lastWeek = (int)Days.Sun;
		Console.WriteLine("Thu hai: {0}", firstWeek);
        Console.WriteLine("Chu nhat: {0}", lastWeek);
		Console.ReadKey();
	}
}