using System;
					
public class Fraction{
	public int tu;
	public int mau;
	public void Input(){
		Console.Write("Nhập tử số:\t");
		this.tu = Int32.Parse(Console.ReadLine());
		do{
		Console.Write("Nhập mẫu số:\t");
		this.mau = Int32.Parse(Console.ReadLine());
		}
		while(this.mau == 0);
	}
	public void Output(){
		this.ShowFraction();
	}
	public int FindGcd(int tu, int mau){
		return (mau == 0) ? tu : FindGcd(mau, tu%mau);
	}
	public void ShowFraction(){
		int resultTu = this.tu/this.FindGcd(this.tu, this.mau);
		int resultMau = this.mau/this.FindGcd(this.tu, this.mau);
		Console.WriteLine("Tử số:\t" + resultTu);
		Console.WriteLine("Mẫu số:\t" + resultMau);
		if(resultTu == 0){
			Console.WriteLine("Value of Fraction:\t" + 0);
		}
		else{
		if(resultTu == resultMau)
			Console.WriteLine("Value of Fraction:\t" + 1);
		else{
			Console.WriteLine("Value of Fraction:\t" + resultTu + "/" + resultMau);
		}
		}
		
	}
	public static Fraction operator+ (Fraction fractionOne ,Fraction fractionTwo){
		Fraction result = new Fraction();
		result.mau = fractionOne.mau * fractionTwo.mau;
		result.tu = fractionOne.tu *fractionTwo.mau + fractionOne.mau * fractionTwo.tu ;
		return result;
	}
	public static Fraction operator- (Fraction fractionOne ,Fraction fractionTwo){
		Fraction result = new Fraction();
		result.mau = fractionOne.mau * fractionTwo.mau;
		result.tu = fractionOne.tu * fractionTwo.mau - fractionOne.mau * fractionTwo.tu ;
		return result;
	}
	public static Fraction operator* (Fraction fractionOne ,Fraction fractionTwo){
		Fraction result = new Fraction();
		result.mau = fractionOne.mau * fractionTwo.mau;
		result.tu = fractionOne.tu * fractionTwo.tu ;
		return result;
	}
	public static Fraction operator/ (Fraction fractionOne ,Fraction fractionTwo){
		Fraction result = new Fraction();
		result.tu = fractionOne.tu * fractionTwo.mau ;
		result.mau = fractionOne.mau * fractionTwo.tu;
		return result;
	}
}

public class Program
{
	public static void Main()
	{
		Fraction p0 = new Fraction();
		Fraction p1 = new Fraction();
		Fraction addObject = new Fraction();
		Fraction subObject = new Fraction();
		Fraction mulObject = new Fraction();
		Fraction divObject = new Fraction();
		p0.Input();
		p1.Input();
		p0.Output();
		p1.Output();
		addObject = p0 + p1;
		subObject = p0 - p1;
		mulObject = p0 * p1;
		divObject = p0 / p1;
		addObject.Output();
		subObject.Output();
		mulObject.Output();
		divObject.Output();
		Console.ReadKey();
	}
}