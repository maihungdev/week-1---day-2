using System;
	
struct BookDetail{
	public string nameBook;
	public string nameAuthor;
	public string Category;
	public int codeBook;
}

public class Program
{
	public static void Main()
	{
		Console.WriteLine("Struct trong C#");
        Console.WriteLine("----------------------------\n");
		BookDetail book1;
		BookDetail book2;
		
		/* thong tin chi tiet ve Book1 */
		book1.nameBook = "C# Head First";
		book1.nameAuthor = "John Devil";
		book1.Category = "IT Book";
		book1.codeBook = 1;
		
		/* thong tin chi tiet ve Book2 */
		book2.nameBook = "WPF UI Design";
		book2.nameAuthor = "Eveil Ken";
		book2.Category = "IT Book";
		book2.codeBook = 2;
		
		/* in cac thong tin cua Book1*/
        Console.WriteLine("In thong tin cua cuon sach 1:");
		Console.WriteLine("Ten sach: {0}", book1.nameBook);
		Console.WriteLine("Tac gia: {0}", book1.nameAuthor);
		Console.WriteLine("The loai: {0}", book1.Category);
		Console.WriteLine("Ma sach: {0}", book1.codeBook);

        /* in cac thong tin cua Book2 */
        Console.WriteLine("\nIn thong tin cua cuon sach 2:");
        Console.WriteLine("Ten sach: {0}", book2.nameBook);
        Console.WriteLine("Tac gia: {0}", book2.nameAuthor);
        Console.WriteLine("The loai: {0}", book2.Category);
        Console.WriteLine("Ma sach: {0}", book2.codeBook);

        Console.ReadKey();
		
	}
}