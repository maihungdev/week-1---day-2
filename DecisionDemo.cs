using System;
					
public class Program
{
	public static void Main()
	{
		int a, b, c;
		float delta;
		Console.WriteLine("Nhập a, b, c:");
		a = Int32.Parse(Console.ReadLine());
		b = Int32.Parse(Console.ReadLine());
		c = Int32.Parse(Console.ReadLine());
		delta = b*b - 4*a*c;
		if(a==0){
			if(b==0){
				if(c==0){
					Console.WriteLine("Phuong trinh vo so nghiem");
				}
				else{
					Console.WriteLine("Phuong trinh vo nghiem");
				}
			}
			else{
				Console.WriteLine("Phuong trinh co nghiem la:\t" + (double)-c/b);
			}
		}
		else if(a+b+c==0){
			Console.WriteLine("Phuong trinh co nghiem kep:\t" + "1 va " + (double)-c/a);
		}
		else if(a-b+c == 0){
			Console.WriteLine("Phuong trinh co nghiem kep:\t" + "-1 va " + (double)-c/a);
		}
		else{
		if(delta < 0)
		{
			Console.WriteLine("Phuong trinh vo nghiem");
		}
		else if(delta == 0){
			Console.WriteLine("Phuong trinh co nghiem:\t" + (double)(-b/2*a));
		}
		else{
			Console.WriteLine("Phuong trinh co nghiem kep:\t" + (double)(-b-Math.Sqrt(delta)/2*a) + " va " + (double)(-b+Math.Sqrt(delta)/2*a));
		}
		}
	}
}